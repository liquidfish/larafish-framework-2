<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use \Liquidfish\Larafish\Page\Page;
use \Liquidfish\Larafish\Page\Version;
use Liquidfish\Larafish\Page\Component\Component as PageComponent;
use Liquidfish\Larafish\Models\Permission;

class AddPublishing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page_versions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id', false, true);
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
            $table->enum('status', ['active', 'draft']);
            $table->timestamps();
        });

        Schema::create('publishing_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('page_id', false, true);
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
            $table->integer('author_id');
            $table->integer('publisher_id');
            $table->text('comments');
            $table->enum('status', ['SUBMISSION', 'PUBLISHED', 'DECLINED']);
            $table->timestamps();
        });

        Schema::table('page_components', function($table)
        {
            $table->integer('version_id', false, true);
            $table->foreign('version_id')->references('id')->on('page_versions')->onDelete('cascade');
        });

        // The following logic populates the fields and tables we've just added.

        $pages = Page::all();

        foreach($pages as $page) {
            $version = Version::create(array('page_id' => $page->id, 'status' => 'active'));

            $components = PageComponent::where('page_id', $page->id)->get();

            foreach($components as $component) {
                $component->version_id = $version->id;
                $component->save();
            }
        }

        Permission::create(array('name' => 'author', 'display_name' => 'Author'));
        Permission::create(array('name' => 'publish', 'display_name' => 'Publish'));

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Ain't no going back.

        //Schema::drop('page_versions');
    }
}
