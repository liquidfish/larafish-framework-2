<?php

use Illuminate\Database\Migrations\Migration;

class CreatePageComponentDataTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('page_component_data',function($t)
		{
			$t->engine = "InnoDB";
			$t->increments('id');
			$t->text('data');
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('page_component_data');
	}

}