<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserPasswordResetsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_password_resets', function($table)
		{
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->string('code', 32)->unique();
			$table->integer('user_id')->unsigned();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_password_resets');
	}

}