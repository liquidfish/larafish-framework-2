<?php

use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('pages',function($t)
		{
			$t->engine = 'InnoDB';
			$t->increments('id');
			$t->string('primary_uri',75)->nullable();
			$t->string('secondary_uri',75)->nullable();
			$t->string('tertiary_uri',75)->nullable();
			$t->string('title',75);
			$t->string('nav_title',75);
			$t->string('view',50)->nullable();
			$t->boolean('published')->default(0);
			$t->string('redirect_url')->nullable();
			$t->boolean('locked')->default(0);
			$t->text('content')->nullable();
			$t->text('meta_tags')->nullable();
			$t->text('extra_body_classes')->nullable();
			$t->boolean('protected')->default(0);
			$t->integer('priority')->default(0);
			$t->unique(array('primary_uri','secondary_uri','tertiary_uri'));
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('pages');
	}

}