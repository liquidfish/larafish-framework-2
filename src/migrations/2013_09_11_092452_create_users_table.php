<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('users', function($t)
		{
			$t->engine = 'InnoDB';
			$t->increments('id');
			$t->string('first_name', 50);
			$t->string('last_name', 50);
			$t->string('username', 255)->unique();
			$t->string('password', 64);
			$t->string('remember_token', 100)->nullable();
			$t->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::drop('users');
	}

}