<?php namespace Liquidfish\Larafish\Commands;

use Carbon\Carbon;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BackupCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'larafish:backup';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Back up the database and the uploads directory';

	/**
	 * @var
	 */
	private $filesystem;

	/**
	 * @var string
	 */
	private $backupPath;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(Filesystem $filesystem)
	{
		parent::__construct();
		$this->filesystem = $filesystem;
		$this->backupPath = base_path().'/app/storage/backups';
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->ensureBackupDirectoryExists();

		$username = getenv('DB1_USER');
		$password = trim('-p'.getenv('DB1_PASS'));
		if(strlen($password) == 2) $password = '';

		$database = getenv('DB1_NAME');

		$date = Carbon::now()->format('Y-m-d');

		# Create dump
		$process = new Process("mysqldump -u $username $password $database > {$this->backupPath}/$database-database_$date.sql");
		$process->run();

		if (!$process->isSuccessful()) {
			throw new \RuntimeException($process->getErrorOutput());
		}

		# Get comma seperated directories to back up (absolute paths)
		$directories = $this->option('directory');

		foreach($directories as $index => $dir)
		{
			if(!file_exists($dir))
			{
				unset($directories[$index]);
				$this->info("$dir does not exist");
			}
		}

		$compressProcess = new Process("tar -zcvf {$this->backupPath}/{$database}-files_$date.tgz ".join(' ', $directories));
		$compressProcess->run();

		if (!$process->isSuccessful()) {
			throw new \RuntimeException($compressProcess->getErrorOutput());
		}

		$this->deleteTwoWeekOldBackups();

		$this->info('Created backup');

	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			 array('directory', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Additional directory to back up.', null),
		);
	}

	/**
	 * Ensure the backup directory exists
	 */
	private function ensureBackupDirectoryExists()
	{
		if(!$this->filesystem->exists($this->backupPath))
		{
			$this->filesystem->makeDirectory($this->backupPath, 0755);
		}
	}

	/**
	 * Check for files we need to delete
	 */
	private function deleteTwoWeekOldBackups()
	{
		$cutoff = Carbon::now()->subWeeks(2);

		$filesToDelete = array();

		$files = $this->filesystem->files($this->backupPath);

		foreach($files as $file)
		{
			$fileDateString = substr($file, -14, 10);

			$fileDate = Carbon::createFromTimestamp(strtotime($fileDateString));

			if(!$fileDate->gte($cutoff))
			{
				$filesToDelete[] = $file;
			}
		}

		$this->filesystem->delete($filesToDelete);
	}

}
