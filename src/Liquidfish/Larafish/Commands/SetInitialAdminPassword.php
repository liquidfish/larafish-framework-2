<?php namespace Liquidfish\Larafish\Commands;

use Illuminate\Console\Command;
use Liquidfish\Larafish\Models\User;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SetInitialAdminPassword extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'larafish:password';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Set up initial admin password.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$password = $this->option('password');
		if(is_null($password))
		{
			$password = str_random(8);
		}

		try
		{
			$user = User::where('username', 'dev@liquidfish.com')->first();
			$user->password = bcrypt($password);
			$user->save();

			$this->info('+ /admin password for dev@liquidfish.com = '.$password);
		}
		catch (\Exception $e)
		{
			$this->error('- Not able to set dev@liquidfish.com password. Please use php artisan larafish:password to set a new password');
		}

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
//			 array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			 array('password', null, InputOption::VALUE_OPTIONAL, 'The password you wish to use', null),
		);
	}

}
