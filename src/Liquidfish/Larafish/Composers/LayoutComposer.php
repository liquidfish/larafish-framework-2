<? namespace Liquidfish\Larafish\Composers;

use App;
use Assets;
use Config;
use Session;
use View;
use Larafish;

class LayoutComposer {

	public function compose($view)
	{
		# Check for some defaults
		if(!$view->title) $view->title = 'Page';
		if(!$view->body_classes) $view->body_classes = '';

		if(Larafish::userHasRole('Admin') or Larafish::userHasRole('Architect') or Larafish::userCan('author') or Larafish::userCan('publish'))
		{
			Assets::addNamed('semantic-css','jquery','ckeditor','admin','semantic-js');

			# Add admin navigation
			$view->admin_navigation = View::make('larafish::admin.navigation');

//			# Add publishing notifications
//			$view->publishing_notifications = View::make('larafish::admin.publishing');
		}

		if(Larafish::userHasRole('Admin')) $view->body_classes .= ' admin';
		if(Larafish::userHasRole('Architect')) $view->body_classes .= ' architect';

		if(Session::has('message') or Session::has('success_message') or Session::has('failure_message'))
		{
			$view->message = View::make('larafish::admin.message')->withMessageDetails(Session::get('message_details'));

			if(Session::has('message'))
			{
				$view->message->withMessage(Session::get('message'));
			}

			if(Session::has('success_message'))
			{
				$view->message->withSuccess(true)->withMessage(Session::get('success_message'));
			}

			if(Session::has('failure_message'))
			{
				$view->message->withSuccess(false)->withMessage(Session::get('failure_message'));
			}
		}


	}
}
