<? namespace Liquidfish\Larafish\Composers;

use App;
use View;
use InvalidArgumentException;
use Illuminate\Filesystem\Filesystem;

class PageSettingsComposer {

	public function compose($view)
	{

		$available_views = array('larafish::pages.default' => 'Larafish Default');

		try {

			$fs = new Filesystem();
			$page_views = $fs->allFiles(base_path().'/resources/views/pages');

			foreach($page_views as $page_view)
			{
				$v = str_replace(array(base_path().'/resources/views/pages/','.php'), '', $page_view->getRelativePathname());
				$v = str_replace('/', '.', $v);
				$available_views[$v] = $v;
			}

		} catch (InvalidArgumentException $e) {

			$view->with('message', 'Please create the '.base_path().'/resources/views/pages directory for your own page views.');

		}

		$view->with('available_views', $available_views);

	}

}