<?php namespace Liquidfish\Larafish\Models;

class UserPasswordReset extends \Eloquent {
	
	protected $guarded = array('id');

	public function user()
	{
		return $this->belongsTo('Liquidfish\Larafish\Models\User','user_id');
	}

	public static function boot()
	{
		parent::boot();

		/**
		 * Create the name of the Permission from the display name
		 */
		static::saving(function($reset)
		{
			$reset->code = str_random(32);
		});

	}

}