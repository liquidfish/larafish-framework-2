<?php namespace Liquidfish\Larafish\Routing;

use App, Cache, Input, Route, Redirect;

class DefaultRoutes {

	/**
	 * Default routes for Larafish
	 *
	 * This allows us to overcome first-in first-out routes by
	 * letting the developer call this class to load in these
	 * routes in at the bottom of app/routes.php
	 *
	 * @return void
	 */
	public static function all()
	{

		# Here we use the project's PageController, in case they would like to override functionality
		Route::get('/', array('as' => 'home', 'uses' => config('larafish.pages.controller').'@index'));

		# Sign In
		Route::get('login', array('as' => 'login', 'uses' => 'Liquidfish\Larafish\Controllers\LoginController@getIndex'));
		Route::post('login', 'Liquidfish\Larafish\Controllers\LoginController@postIndex');

		# Password Reset
		Route::get('forgot-password', array('as' => 'forgot-password', 'uses' => 'Liquidfish\Larafish\Controllers\PasswordResetController@index'));
		Route::post('forgot-password/reset', array('as' => 'password-reset-request', 'uses' => 'Liquidfish\Larafish\Controllers\PasswordResetController@resetRequest'));
		Route::get('forgot-password/reset/{code}', array('as' => 'password-reset-url', 'uses' => 'Liquidfish\Larafish\Controllers\PasswordResetController@resetForm'));
		Route::post('forgot-password/reset/{code}', array('as' => 'password-reset', 'uses' => 'Liquidfish\Larafish\Controllers\PasswordResetController@reset'));

		Route::get('search', 'Liquidfish\Larafish\Search\Controller@search');

		Route::group(array('prefix' => 'admin', 'middleware' => 'auth'), function () {
			Route::get('logout', array('as' => 'logout', 'uses' => 'Liquidfish\Larafish\Controllers\LoginController@getLogout'));
		});


		Route::group(array('prefix' => 'admin', 'middleware' => 'author'), function ()
		{
			# Versions / Publishing

			Route::post('versions/submit', 'Liquidfish\Larafish\Page\Version\Controller@submitDraft');
			Route::post('versions/unsubmit', 'Liquidfish\Larafish\Page\Version\Controller@unsubmitDraft');
			Route::post('versions/create', 'Liquidfish\Larafish\Page\Version\Controller@createDraft');
			Route::post('versions/publish', 'Liquidfish\Larafish\Page\Version\Controller@publishDraft');
			Route::post('versions/delete', 'Liquidfish\Larafish\Page\Version\Controller@deleteDraft');
			Route::post('versions/decline', 'Liquidfish\Larafish\Page\Version\Controller@declineDraft');
			Route::post('versions/deleteNotification', 'Liquidfish\Larafish\Page\Version\Controller@deleteNotification');

			# Page Components
			Route::put('pages/components/data/update','Liquidfish\Larafish\Controllers\ComponentDataController@update');
		});

		Route::group(array('prefix' => 'admin', 'middleware' => 'admin'), function()
		{
			Route::get('/',function()
			{
				return Redirect::route('home');
			});

			# Page Roles
			Route::post('pages/roles','Liquidfish\Larafish\Page\Role\Controller@store');
			Route::delete('pages/roles','Liquidfish\Larafish\Page\Role\Controller@destroy');

			# Pages
			Route::put('pages/update-priority','Liquidfish\Larafish\Page\AdminController@updatePriority');
			Route::post('pages/components','Liquidfish\Larafish\Page\Component\Controller@store');
			Route::delete('pages/components','Liquidfish\Larafish\Page\Component\Controller@destroy');
			Route::resource('pages','Liquidfish\Larafish\Page\AdminController');
			Route::resource('users','Liquidfish\Larafish\Controllers\UserController');
			Route::resource('roles','Liquidfish\Larafish\Controllers\RoleController');
			Route::resource('permissions','Liquidfish\Larafish\Controllers\PermissionController');
			Route::get('components/sync','Liquidfish\Larafish\Component\Controller@sync');
			Route::resource('components','Liquidfish\Larafish\Component\Controller');

			# Files
			Route::get('files/browser', array('as' => 'admin.files.browser', 'uses' => 'Liquidfish\Larafish\Controllers\FileController@browser'));
			Route::post('files/upload','Liquidfish\Larafish\Controllers\FileController@upload');
			Route::post('files/create-directory','Liquidfish\Larafish\Controllers\FileController@createDirectory');
			Route::get('files/delete-directory/{id}', array('as' => 'admin.files.delete-directory', 'uses' => 'Liquidfish\Larafish\Controllers\FileController@deleteDirectory'));
			Route::post('files/change', array('as' => 'admin.files.change', 'uses' => 'Liquidfish\Larafish\Controllers\FileController@change'));
			Route::resource('files','Liquidfish\Larafish\Controllers\FileController');
		});

		Route::group(array('prefix' => 'lf'), function()
		{

			Route::get('map',function()
			{
				$navigator = App::make('Liquidfish\Larafish\Page\Navigation\Navigator');
				$string = '<pre>';
				$string .=  print_r($navigator->map(), true);
				$string .= '</pre>';
				return $string;
			});

			Route::get('navigation',function()
			{
				$navigator = App::make('Liquidfish\Larafish\Page\Navigation\Navigator');
				return $navigator->markup();
			});

			Route::get('clear-cache',function()
			{
				if(Input::get('key'))
				{
					Cache::forget(Input::get('key'));
				}
				var_dump(Cache::forget('pages'));
				var_dump(Cache::forget('navigation'));
			});

		});

		# Catch all for pages
		Route::get('{primary_uri}/{secondary_uri?}/{tertiary_uri?}', array('as' => 'page', 'uses' => config('larafish.pages.controller').'@index'));

	}

}