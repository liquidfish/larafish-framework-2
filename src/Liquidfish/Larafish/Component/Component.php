<?php namespace Liquidfish\Larafish\Component;

use Str;

class Component extends \Eloquent {
	public $timestamps = false;
	protected $guarded = array('id');
	protected $appends = array('title');

	public function getTitleAttribute()
	{
		return str_slug( str_replace(array('::','.'), '-', $this->view) );
	}
}