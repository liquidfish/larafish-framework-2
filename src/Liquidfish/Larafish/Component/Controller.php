<?php namespace Liquidfish\Larafish\Component;

# Sync components
# larafish and app

# add to page
use DB;
use InvalidArgumentException;
use Redirect;
use View;
use Illuminate\Filesystem\Filesystem;
use Liquidfish\Larafish\Component\Component;
use Liquidfish\Larafish\Page\BaseController;
use Liquidfish\Larafish\Larafish;

class Controller extends BaseController {

	protected $larafish;
	protected $component;

	public function __construct(Larafish $larafish, Component $component)
	{
		$this->larafish = $larafish;
		$this->component = $component;
	}

	public function index()
	{
		$this->layout->yield = View::make('larafish::admin.components.list')->with('components', $this->component->paginate(20));
	}

	public function sync()
	{

		$available_views = array('larafish::components.content-area' => 'larafish::components.content-area');

		try {

			$fs = new Filesystem();
			$component_views = $fs->allFiles(base_path().'/resources/views/components');

			foreach($component_views as $component_view)
			{
				$v = str_replace(array(base_path().'/resources/views/components/','.php'), '', $component_view->getRelativePathname());
				$v = str_replace('/', '.', $v);
				$available_views[$v] = $v;
			}

			$components = DB::table('components')->lists('view','view');

			$components_to_create = array_diff($available_views,$components);
			$components_to_delete = array_diff($components,$available_views);

			foreach($components_to_create as $component => $view)
			{
				$this->component->create(array('view' => $view));
			}
			# Remove components
			$message_details = array();
			if(count($components_to_create))
			{
				$message_details[] = 'Created: '.implode(', ', array_values($components_to_create));
			}
			if(count($components_to_delete))
			{
				DB::table('components')->whereIn('view', array_values($components_to_delete))->delete();
				$message_details[] = 'Deleted: '.implode(', ', array_values($components_to_delete));
			}
			$message = 'Updated components';

		} catch (InvalidArgumentException $e) {
			
			$message = 'Please create the '.base_path().'/resources/views/pages directory for your own page views.';

		} catch (Exception $e) {
			$message = $e->getMessage();
		}

		return Redirect::route('admin.components.index')->with('message', $message)->with('message_details', $message_details);
	}

}