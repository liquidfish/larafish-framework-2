<?php namespace Liquidfish\Larafish;

use Illuminate\Support\ServiceProvider;

class LarafishServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->loadViewsFrom(__DIR__.'/../../views', 'larafish');

		$this->publishes([
			__DIR__.'/../../config/config.php' => config_path('larafish.php'),
		]);

		$this->publishes([
			__DIR__.'/../../config/bugsnag.php' => config_path('bugsnag.php'),
		]);

		$this->publishes([
			__DIR__.'/../../../public/assets' => public_path('vendor/larafish'),
		], 'public');

		# Make migrations publishable
		$this->publishes([
			__DIR__.'/../../migrations/' => database_path('migrations')
		], 'migrations');

		$this->mergeConfigFrom(
			__DIR__.'/../../config/config.php', 'larafish'
		);

		# Observers
		Page\Page::observe(new Page\Observer);

		# Load Filters
//		include __DIR__.'/../../filters.php';
		include __DIR__.'/../../composers.php';

	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		# Register bugsnag exception handler
		$this->app->alias('bugsnag.logger', \Illuminate\Contracts\Logging\Log::class);
		$this->app->alias('bugsnag.logger', \Psr\Log\LoggerInterface::class);

		// Register 'assets' instance container to our AssetController object
		$this->app->bind(
			'assets',
			function()
			{
				return new \Liquidfish\Larafish\Controllers\AssetController;
			},
			true
		);

		$this->app->bind(
			'larafish',
			function()
			{
				return new \Liquidfish\Larafish\LarafishPublishing;
			},
			true
		);

		# Shortcut so developers don't need to add an Alias in app/config/app.php

		$loader = \Illuminate\Foundation\AliasLoader::getInstance();

		$this->app->booting(function() use ($loader)
		{
			$loader->alias('Assets', 'Liquidfish\Larafish\Facades\Assets');
			$loader->alias('Larafish', 'Liquidfish\Larafish\Facades\Larafish');
		});

		# Add Initial Setup command
		$this->app['command.larafish.setup'] = $this->app->share(function($app)
		{
			return new Commands\InitialSetupCommand;
		});
		$this->commands('command.larafish.setup');

		# Add Initial Setup command
		$this->app['command.larafish.password'] = $this->app->share(function($app)
		{
			return new Commands\SetInitialAdminPassword;
		});
		$this->commands('command.larafish.password');

		# Add Backup Setup command
		$this->app['command.larafish.backup'] = $this->app->share(function($app)
		{
			return new Commands\BackupCommand(new \Illuminate\Filesystem\Filesystem);
		});
		$this->commands('command.larafish.backup');

	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('assets', 'larafish');
	}

}
