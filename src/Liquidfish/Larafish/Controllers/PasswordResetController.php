<?php namespace Liquidfish\Larafish\Controllers;

use Exception;
use Input;
use Mail;
use Redirect;
use URL;
use View;
use Liquidfish\Larafish\Models\User;
use Liquidfish\Larafish\Models\UserPasswordReset;

class PasswordResetController extends \Liquidfish\Larafish\Page\Controller {

	public function index()
	{
		$this->layout->title = 'Forgot Password';
		$this->layout->yield = view('larafish::forgot-password');
	}

	public function resetRequest()
	{
		try {
			
			$username = Input::get('username');

			$user = User::where('username','=',$username)->firstOrFail();
			$reset = UserPasswordReset::create(array('user_id' => $user->id));

			$reset_url = route('password-reset-url', $reset->code);

			if(filter_var($user->username, FILTER_VALIDATE_EMAIL)) {
				
				Mail::send('larafish::emails.password-reset', array('reset_url' => $reset_url), function($message) use ($user)
				{
					$message->to($user->username)->subject('Reset your password here');
				});

			}
			else {
				$message = 'The username is not a valid email address';
			}

			$message = 'Password reset link sent!';

		} catch (Exception $e) {
			
			$message = 'Password reset link sent !';
			
		}

		return redirect()->route('forgot-password')->with('message', $message);
	}

	public function reset($code)
	{
		try {
			
			$input = Input::only('password','confirm_password');

			if($input['password'] === $input['confirm_password'] and $input['password'] != '')
			{
				$reset = UserPasswordReset::where('code','=',$code)->with('user')->firstOrFail();

				$reset->user->password = $input['password'];
				$reset->user->save();

				$reset->delete();

			}
			else
			{
				throw new Exception('Passwords do not match');
			}

			return redirect()->route('login')->with('message','New password saved! Please login');

		} catch (Exception $e) {
			
			return redirect()->route('login')->with('message', $e->getMessage());

		}
	}

	public function resetForm($code)
	{

		try {

			$reset = UserPasswordReset::where('code','=',$code)->firstOrFail();

			$this->layout->title = 'Reset your password';
			$this->layout->yield = view('larafish::password-reset')->with('code',$code);

		} catch (Exception $e) {
			
			return redirect()->route('forgot-password')->with('message', 'Reset expired');

		}

	}


}
