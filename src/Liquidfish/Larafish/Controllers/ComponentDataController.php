<?php namespace Liquidfish\Larafish\Controllers;

use DB, Exception, Input, Response, Request;
use Liquidfish\Larafish\Page\Repository as PageRepository;
use Liquidfish\Larafish\Page\Component\Data\Data as PageComponentData;

class ComponentDataController extends \Illuminate\Routing\Controller {

	private $pageRepository;

	function __construct(PageRepository $pageRepository)
	{
		$this->pageRepository = $pageRepository;
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return \Response
	 */
	public function update()
	{
		//
		
		$response = array('success' => 0);
		
		if(Request::ajax())
		{
			try
			{
				$input = Input::get('components');
				$components = json_decode($input,true);

				# Update page data, typically used for search
				$page_id = Input::get('page_id',0);
				if($page_id != 0)
				{
					$content = preg_replace( '/\s+/', ' ', Input::get('page_content') );
					DB::table('pages')->where('id',$page_id)->update(array('content' => $content));
				}

				foreach($components as $component)
				{
					$page_component = PageComponentData::find($component['id']);

					if(isset($component['id']))
					{
						unset($component['id']);
					}

					$json = json_encode($component);
					$page_component->data = $json;

					$this->pageRepository->clearCacheById($page_id);

					$page_component->save();
				}
			
				$response['success'] = 1;
		
			} catch (Exception $e) {
		
				$response['message'] = $e->getMessage();
		
			}
		}
		
		return Response::json($response);
		
	}

}