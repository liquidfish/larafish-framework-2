<?php

namespace Liquidfish\Larafish\Controllers;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Liquidfish\Larafish\Models\User;
use Liquidfish\Larafish\Operation\Operation;

class LoginController extends \Liquidfish\Larafish\Page\Controller {

	public function getIndex()
	{
		$this->layout->title = 'Login';
		$this->layout->page = 'login';
		$this->layout->yield = view('larafish::login');
	}

	/**
	 * @return mixed
	 */
	public function postIndex()
	{
		$operation = new Operation();

		// Create body
		$credentials = [
			"username" => Request::get('username'),
			"password" => Request::get('password'),
		];

		$validator = Validator::make($credentials, [
			'username' => 'required|email',
			'password' => 'required'
		]);

		if($validator->passes())
		{
			if($this->isLiquidfishUsername($credentials['username']))
			{
				if($this->remoteLogin($credentials))
				{
					$user = User::where('username', $credentials['username'])->first();

					if(! is_null($user))
					{
						Auth::login($user);
						$operation->succeeded();
					}
					else
					{
						$operation->setMessage('No fish caught.');
					}
				}
				else
				{
					$operation->setMessage('Please fish again.');
				}
			}
			else
			{
				if (Auth::attempt($credentials))
				{
					$this->recordLogin($credentials['username']);
					$operation->succeeded();
				} else {
					$operation->setMessage('Your username or password was incorrect');
				}
			}
		}
		else
		{
			$operation->setMessage('Please provide a valid username and password');
		}

		if($operation->wasSuccessful())
		{
//			return Redirect::intended('/');
			return Redirect::to('/');
		}
		else
		{
			return Redirect::to('/login')->with('message', $operation->getMessage());
		}
	}

	/**
	 * @param $username
	 * @return bool
	 */
	protected function isLiquidfishUsername($username)
	{
		return (str_contains($username, '@liquidfish.com') || str_contains($username, '@liquid.fish'));
	}

	/**
	 * @param array $credentials
	 * @return bool
	 */
	protected function remoteLogin(array $credentials)
	{
		$success = false;

		// Get cURL resource
		$ch = curl_init();

		// Set url
		curl_setopt($ch, CURLOPT_URL, 'https://tank.liquid.fish/remote-login?key='.env('TANK_KEY'));

		// Set method
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');

		// Set options
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$server = 'unknown';
		if(isset($_SERVER['SERVER_NAME']))
		{
			$server = $_SERVER['SERVER_NAME'];
		}

		// Set headers
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
				"Referer: ".$server,
				"Content-Type: application/x-www-form-urlencoded; charset=utf-8",
			]
		);

		$ip = 'unknown';
		if(isset($_SERVER['REMOTE_ADDR']))
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		$credentials = http_build_query(array_merge([ 'ip' => $ip ], $credentials));

		// Set body
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $credentials);

		// Send the request & save response to $resp
		$resp = curl_exec($ch);

		if($resp)
		{
			$json = json_decode($resp);
			if(! is_null($json))
			{
				if($json->success === true)
				{
					$success = true;
				}
			}
		}

		// Close request to clear up some resources
		curl_close($ch);

		return $success;
	}

	/**
	 * Record successful login information to local log file
	 * @param $username
	 */
	protected function recordLogin($username)
	{
		try
		{
			File::append(storage_path('logs/ip.txt'), $username.' - '.Request::getClientIp(true).' - '.date('Y-m-d g:i:s a')."\n");
		}
		catch (Exception $e){}
	}

	public function getLogout()
	{
		Auth::logout();
		Session::flush();
		return Redirect::route('home');
	}

}