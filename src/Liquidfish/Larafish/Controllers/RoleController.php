<?php namespace Liquidfish\Larafish\Controllers;

use Input;
use View;
use Liquidfish\Larafish\Models\Role;
use Liquidfish\Larafish\Models\Permission;

class RoleController extends \Liquidfish\Larafish\Page\Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$roles = Role::orderBy('id','desc')->with('permissions');

		if(Input::has('query'))
		{
			$roles->where('name','LIKE','%'.Input::get('query').'%');
		}

		$this->layout->title = "Roles";
		$this->layout->yield = View::make('larafish::admin.roles.list')->with('roles', $roles->paginate(20));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		// Need to get available roles and permissions
		
		$this->layout->title = 'Create Role';
		$roles = Role::all();
		$permissions = Permission::all();
		$this->layout->yield = View::make('larafish::admin.roles.create')->with('roles',$roles)->with('permissions',$permissions);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
		# Create Role
		$role = Role::create(Input::except('perms'));

		# Add permissions
		$permissions = Input::get('perms');
		if(count($permissions))
		{
			$role->permissions()->sync($permissions);
		}

		return \Redirect::route('admin.roles.edit',array($role->id))->with('message','Role Created!');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
		$this->layout->title = 'Edit Role';
		$role = Role::find($id);
		$role->load('permissions');

		$permissions = Permission::all();

		$this->layout->yield = View::make('larafish::admin.roles.edit')->with('role',$role)->with('permissions',$permissions);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
		$role = Role::find($id);
		$role->fill(Input::except('_method','perms'));
		$role->save();

		# Sync permissions
		$permissions = Input::get('perms');
		if(count($permissions))
		{
			$role->permissions()->sync($permissions);
		}

		return \Redirect::route('admin.roles.edit',array($role->id))->with('message','Updated Role');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
		$user = Role::find($id);
		$user->delete();
		
		return \Redirect::route('admin.users.index')->with('message','Role deleted');
	}

}