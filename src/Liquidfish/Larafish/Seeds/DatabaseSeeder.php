<?php namespace Liquidfish\Larafish\Seeds;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		\Eloquent::unguard();

		$this->call('Liquidfish\Larafish\Seeds\UserTableSeeder');
		$this->call('Liquidfish\Larafish\Seeds\PageTableSeeder');
	}

}
