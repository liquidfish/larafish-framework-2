<?php namespace Liquidfish\Larafish\Seeds;

use DB;
use Eloquent;
use Liquidfish\Larafish\Component\Component;
use Liquidfish\Larafish\Page\Page;
use Illuminate\Database\Seeder;

class PageTableSeeder extends Seeder {

	public function run()
	{

		Eloquent::unguard();

		DB::table('components')->delete();

		Component::create(
			array(
				'id' => 1,
				'view' => 'larafish::components.content-area'
			)
		);

		DB::table('pages')->delete();

		$homepage = Page::create(
			array(
				'primary_uri' => null,
				'secondary_uri' => null,
				'tertiary_uri' => null,
				'title' => 'Homepage',
				'published' => 1
			)
		);

		$test = Page::create(
			array(
				'primary_uri' => 'test',
				'secondary_uri' => null,
				'tertiary_uri' => null,
				'title' => 'Test Page',
				'published' => 1
			)
		);

	}

}
