<?php namespace Liquidfish\Larafish\Seeds;

use Liquidfish\Larafish\Models\User;
use Liquidfish\Larafish\Models\Role;
use Liquidfish\Larafish\Models\Permission;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {

	public function run()
	{
		\DB::table('users')->delete();

		$user = User::create(
			array(
				'first_name' => 'Liquid',
				'last_name' => 'Fish',
				'username' => 'dev@liquidfish.com',
				'password' => str_random(8),
			)
		);

		/*
		----------------------------------------------------------
			Roles and Permissions
		----------------------------------------------------------
		*/

		$architect = new Role;
		$architect->name = 'Architect';
		$architect->save();

		$admin = new Role;
		$admin->name = 'Admin';
		$admin->save();

		$manage_pages = new Permission;
		$manage_pages->name = 'manage_pages';
		$manage_pages->display_name = 'Manage Pages';
		$manage_pages->save();

		$manage_users = new Permission;
		$manage_users->name = 'manage_users';
		$manage_users->display_name = 'Manage Users';
		$manage_users->save();

		$user->attachRole($admin);
		$user->attachRole($architect);

		$permissions = array($manage_pages->id,$manage_users->id);

		$admin->permissions()->sync($permissions);
		$architect->permissions()->sync($permissions);

	}

}
