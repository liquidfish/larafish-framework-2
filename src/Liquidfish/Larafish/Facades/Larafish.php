<?php namespace Liquidfish\Larafish\Facades;

use Illuminate\Support\Facades\Facade;

class Larafish extends Facade {

	protected static function getFacadeAccessor() { return 'larafish'; }

}