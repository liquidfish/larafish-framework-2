<?php namespace Liquidfish\Larafish\Facades;

use Illuminate\Support\Facades\Facade;

class Assets extends Facade {

	protected static function getFacadeAccessor() { return 'assets'; }

}