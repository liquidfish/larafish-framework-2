<?php namespace Liquidfish\Larafish;

use Liquidfish\Larafish\Page\Page;
use Liquidfish\Larafish\Page\Version\Version;
use Liquidfish\Larafish\Page\Version\Publishing as Notifications;

class LarafishPublishing extends Larafish {

    public function __construct() {
        parent::__construct();
    }

    /**
     * @param $pageid
     * @return bool
     * Checks whether specific user has Author role for specific page
     */

    public function userCanAuthor($pageid)
    {
        if(!$this->loggedIn) return false;

        if ($this->current_user->can('manage_pages')) {
            return true;
        }
        else {
            $page = Page::find($pageid);

            foreach($this->current_user->roles as $role) {
                if($page->roles->contains($role->id)) {
                    foreach($role->permissions as $perm) {
                        if($perm->name == 'author' || $perm->name == 'publish') {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public function userCanEditVersion($versionid)
    {
        if(!$this->loggedIn) return false;

        $version = Version::find($versionid);

        # Checked for locked draft (a notification in submission status).  No one can edit locked drafts.
        if(Notifications::where('page_id', $version->page->id)->where('status', 'SUBMISSION')->exists())
            return false;

        if ($this->current_user->can('manage_pages')) {
            return true;
        }
        else {
            foreach($this->current_user->roles as $role) {
                if($version->page->roles->contains($role->id)) {
                    foreach($role->permissions as $perm) {
                        if($perm->name == 'publish')
                            return true; # Publishers can edit active & draft versions

                        if($perm->name == 'author' && $version->status == 'draft')
                            return true; # Authors can edit draft versions only
                    }
                }
            }
        }

        return false;
    }

    public function userCanPublish($pageid) {
        if ($this->current_user->can('manage_pages')) {
            return true;
        }
        else {
            $page = Page::find($pageid);

            foreach($this->current_user->roles as $role) {
                if($page->roles->contains($role->id)) {
                    foreach($role->permissions as $perm) {
                        if($perm->name == 'publish')
                            return true;
                    }
                }
            }
        }
        return false;
    }

    public function getUserPublisherNotifications() {
        $pageids = NULL;

        foreach($this->current_user->roles as $role) {
            foreach($role->pages as $page) {
                $pageids[] = $page->id;
            }
        }

        $notifications = Notifications::whereIn('page_id', $pageids)->where('status', 'submission')->get();

        return $notifications;
    }

    public function getUserAuthorNotifications() {
        $pageids = NULL;

        foreach($this->current_user->roles as $role) {
            foreach($role->pages as $page) {
                $pageids[] = $page->id;
            }
        }

        $notifications = Notifications::whereIn('page_id', $pageids)->where('author_id', $this->user()->id)->whereIn('status', ['published','declined'])->orderBy('updated_at', 'desc')->get();

        return $notifications;
    }

    public function getUserNotificationCount() {
        $count = $this->getUserAuthorNotifications()->count();

        if($this->userCan('publish'))
            $count += $this->getUserPublisherNotifications()->count();

        return $count;
    }

    public function userIsAuthor() {

        if ($this->current_user->can('manage_pages') || $this->current_user->can('author') || $this->current_user->can('publish')) {
            return true;
        }

        return false;
    }
}