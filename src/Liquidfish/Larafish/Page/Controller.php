<?php namespace Liquidfish\Larafish\Page;

use Larafish, Assets, View, Redirect, Input;
use Liquidfish\Larafish\Page\Navigation\Breadcrumb;
use Liquidfish\Larafish\Page\Version\Publishing;
use Liquidfish\Larafish\Routing\Uri;
use Liquidfish\Larafish\Page\Repository as PageRepository;
use Liquidfish\Larafish\Models\Role;

class Controller extends BaseController {

    public $layout = 'layout';

    public function __construct(Uri $uri, PageRepository $page, Breadcrumb $breadcrumb)
    {
        $this->uri = $uri;
        $this->page = $page;
        $this->breadcrumb = $breadcrumb;
    }

    /**
     * Generate the View and Assets for this Page
     * @return View A view object
     */
    public function generate()
    {
        $uri = $this->uri->determine();

        # Check for draft editing
        $edit = Input::get('edit');

        if($edit != 'draft')
        {
            $edit = 'active';
        }

        # Load our page
        $this->page->forUri($uri, $edit);

        # Get Page Version
        $this->page->version = $this->page->getVersion($this->page->data->id, $edit);

        # Check if page has a draft version
        if(Larafish::userCanAuthor($this->page->data->id))
        {
            $draft_exists = $this->page->doesDraftExist($this->page->data->id);
        }

        # Title
        $this->layout->title = $this->page->data->title;

        # Body Classes
        $this->layout->body_classes = $this->bodyClasses();

        $this->content = $this->contentView();

        if($this->page->data->type !== 'home')
        {
            $this->layout->breadcrumbs = View::make('larafish::breadcrumbs')->with('crumbs', $this->breadcrumb->forPage($this->page->data));
        }

        # Give the Page Content View the Page Data
        $this->content->page = $this->page->data;

        # Give the content view its components
        $this->content->components = $this->page->components;

        # Add page settings for admins

        if(Larafish::userCanAuthor($this->page->data->id))
        {
            $awaiting_approval = false;
            $awaiting_count = Publishing::where('page_id', $this->page->data->id)->where('status', 'SUBMISSION')->count();
            if($awaiting_count)
            {
                $awaiting_approval = true;
            }

            $this->layout->publishing_settings = View::make('larafish::admin.publishing')
                ->with('page', $this->page->data)
                ->with('version', $this->page->version)
                ->with('draft_exists', $draft_exists)
                ->with('awaiting_approval', $awaiting_approval)
                ->with('uri', $uri);

            Assets::appendString('<script>window.page_id = '.$this->page->data->id.';</script>');
        }
        if(Larafish::userCan('manage_pages'))
        {
            $this->layout->page_settings = View::make('larafish::admin.pages.settings')->with('page', $this->page->data)->with('version', $this->page->version);
            $this->layout->page_settings->component_settings = View::make('larafish::admin.pages.component-settings')->with('components', $this->page->components->current())->with('available',$this->page->components->available())->with('page_id', $this->page->data->id)->with('version', $this->page->version)->with('roles', Role::all());
            $this->layout->page_settings->role_settings = View::make('larafish::admin.pages.role-settings')->with('page_id', $this->page->data->id)->with('roles', Role::whereNotIn('id', array(1,2))->get())->with('page_roles', $this->page->data->roles);
        }

        return $this->content;
    }

    /**
     * Generate the body classes for this view
     * @return string
     */
    public function bodyClasses()
    {
        $classes = array();
        if($this->uri->getPrimary() == null)
        {
            $classes[] = 'home';
        }
        else
        {
            $classes[] = trim($this->uri->getPrimary().' '.$this->uri->getSecondary().' '.$this->uri->getTertiary());
        }

        $classes[] = trim($this->page->data->extra_body_classes);

        return implode(' ', $classes);
    }

    /**
     * Create the content view
     * @return View Content view for this page
     */
    public function contentView()
    {
        if(str_contains($this->page->data->view, '::'))
        {
            return View::make($this->page->data->view);
        }
        else
        {
            return View::make('pages.'.$this->page->data->view);
        }
    }

    /**
     * View the page
     * @return void
     */
    public function index()
    {
        $this->layout->yield = $this->generate();
    }
}
