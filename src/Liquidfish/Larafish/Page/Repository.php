<?php namespace Liquidfish\Larafish\Page;

use Cache, Exception, Larafish;
use Illuminate\Support\Facades\DB;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use \Liquidfish\Larafish\Page\Component\Repository as PageComponentRepository;
use Liquidfish\Larafish\Page\Component\ComponentBag;
use Liquidfish\Larafish\Page\Version\Version;


class Repository {

    public $id;
    public $data;
    public $content;
    public $components;
    public $version;

    protected $componentDataRepository;

    public function __construct(ComponentBag $components)
    {
        $this->components = $components;
    }

    /**
     * Load a page with its components
     * @param  string $uri URI for the page
     * @return Repository
     */
    public function forUri($uri, $edit)
    {
        $published_only = true;

        if (Larafish::userIsLoggedIn())
        {
            $published_only = false;
        }

        $this->data = Page::withComponents($uri['primary_uri'],$uri['secondary_uri'],$uri['tertiary_uri'], $published_only, $edit);

        # Build components
        foreach($this->data->components as $component)
        {
            $this->components->add($component->component->view, json_decode($component->data->data,true) + array('component' => $component));
        }

        return $this;
    }

    /**
     * Delete a Page
     * @param int $id
     * @return \Liquidfish\Larafish\Page\Page|\Exception
     */
    public function delete($id)
    {
        try
        {
            DB::beginTransaction();
            $page = Page::find($id);
            $page->delete();

            Version::where('page_id', $id)->delete();

            $this->clearCacheById($page->id);

            DB::commit();
        }
        catch (\Exception $e)
        {
            Bugsnag::notifyException($e);
            DB::rollback();
            return $e;
        }


        return $page;
    }

    /**
     * Get all pages seperated by type
     * @return stdClass
     */
    public function allPagesByType()
    {
        $pages = new \stdClass;

        $pages->primaryPages = Page::whereNotNull('primary_uri')->whereNull('secondary_uri')->whereNull('tertiary_uri')->orderBy('priority','asc')->with('roles')->get();
        $pages->secondaryPages = Page::whereNotNull('secondary_uri')->whereNull('tertiary_uri')->orderBy('priority','asc')->with('roles')->get();
        $pages->tertiaryPages = Page::whereNotNull('tertiary_uri')->orderBy('priority','asc')->with('roles')->get();

        return $pages;
    }

    /**
     * Search page content
     * @param $query
     * @return mixed
     */
    public function search($query)
    {
        return Page::where('content', 'like', '%' . $query . '%')->get();
    }

    /**
     * Get version to pass to views
     * @param $query
     * @return mixed
     */
    public function getVersion($id, $edit)
    {
        $pageversion = Version::where('page_id', $id)->where('status', $edit)->firstOrFail();

        return $pageversion;
    }

    /**
     * Return true if page has a draft version
     * @param $query
     * @return mixed
     */
    public function doesDraftExist($pageid)
    {
        $pageversion = Version::where('page_id', $pageid)->where('status', 'draft')->get();

        if(!$pageversion->isEmpty())
            return true;
        else
            return false;
    }


    /**
     * Clear the cache for a Page by ID
     * @param $id
     * @return bool
     */
    public function clearCacheById($id)
    {
        try
        {
            $page = Page::find($id);

            $page_uri = $page->uri;

            if($page_uri === '/')
            {
                $page_uri = '';
            }

            $uri = 'page:'.$page_uri;

            Cache::forget('pages');
            Cache::forget($uri);
            Cache::forget($uri.':published');

            return true;
        }
        catch(Exception $e)
        {
            return false;
        }
    }

}
