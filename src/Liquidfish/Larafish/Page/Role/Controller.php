<?php namespace Liquidfish\Larafish\Page\Role;

use Input;

class Controller extends \Illuminate\Routing\Controller {

    protected $roleRepository;

    function __construct(Repository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function store()
    {
        $pageId = Input::get('page_id');
        $roleId = Input::get('role_id');

        return $this->roleRepository->add($pageId, $roleId);
    }

    public function destroy()
    {
        $pageId = Input::get('page_id');
        $roleId = Input::get('role_id');

        return $this->roleRepository->remove($pageId, $roleId);
    }

}