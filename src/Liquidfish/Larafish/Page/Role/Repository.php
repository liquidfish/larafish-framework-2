<?php namespace Liquidfish\Larafish\Page\Role;

use DB;
use Liquidfish\Larafish\Page\Page;
use Liquidfish\Larafish\Models\Role as Role;

class Repository {

    protected $pageRepository;

    /**
     * Repository constructor.
     * @param $pageRepository
     */
    public function __construct(\Liquidfish\Larafish\Page\Repository $pageRepository)
    {
        $this->pageRepository = $pageRepository;
    }

    /**
     * Attach a Role to a Page
     * @param $pageId, $roleId
     * @return \Liquidfish\Larafish\Models\Role
     */
    public function add($pageId, $roleId)
    {
        $page = Page::find($pageId);

        $role = Role::find($roleId);

        if(!$page->roles->contains($role->id)) {
            $page->roles()->save($role);
        }

        $this->pageRepository->clearCacheById($page->id);

        return $role;
    }

    /**
     * Detach a Role from a Page
     * @param $pageId, $roleId
     * @return \Liquidfish\Larafish\Models\Role
     */
    public function remove($pageId, $roleId)
    {
        $page = Page::find($pageId);
        $role = Role::find($roleId);

        $page->roles()->detach($roleId);

        $this->pageRepository->clearCacheById($page->id);

        return $role;
    }

}