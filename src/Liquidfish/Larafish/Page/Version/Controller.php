<?php

namespace Liquidfish\Larafish\Page\Version;

use Bugsnag\BugsnagLaravel\Facades\Bugsnag;
use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Liquidfish\Larafish\Larafish;
use Liquidfish\Larafish\Page\Page;
use Liquidfish\Larafish\Routing\Uri;
use Liquidfish\Larafish\Page\Repository as PageRepository;
use Liquidfish\Larafish\Page\Component\Component;
use Liquidfish\Larafish\Page\Component\Data\Data as ComponentData;
use Liquidfish\Larafish\Models\User;

class Controller extends \Illuminate\Routing\Controller {

    public function __construct(Uri $uri, PageRepository $page)
    {
        $this->uri = $uri;
        $this->page = $page;
    }

    public function createDraft() {
        $input = Input::all();

        $page_id = $input['page_id'];
        $raw_uri = explode('/', $input['uri']);

        $uri = [];

        isset($raw_uri[0]) && $raw_uri[0] != '' ? $uri['primary_uri']   = $raw_uri[0] : $uri['primary_uri']   = null;
        isset($raw_uri[1]) && $raw_uri[1] != '' ? $uri['secondary_uri'] = $raw_uri[1] : $uri['secondary_uri'] = null;
        isset($raw_uri[2]) && $raw_uri[2] != '' ? $uri['tertiary_uri']  = $raw_uri[2] : $uri['tertiary_uri']  = null;

        $page = Page::find($page_id);

        # Load our page
        $this->page->forUri($uri, 'active');

        # Get Page Version
        $this->page->version = $this->page->getVersion($page->id, 'active');

        # Check if page already has a draft version

        $draft_exists = $this->page->doesDraftExist($page->id);

        if(!$draft_exists) {

            try
            {
                DB::beginTransaction();

                $version = Version::create(array('page_id' => $page->id, 'status' => 'draft'));

                $components = Component::where('page_id', $page->id)->get();

                foreach($components as $component) {

                    $component_data = ComponentData::find($component->page_component_data_id);
                    $draft_component_data = $component_data->replicate();
                    $draft_component_data->save();

                    $draft_component = new Component;

                    $draft_component->page_component_data_id = $draft_component_data->id;
                    $draft_component->page_id = $page->id;
                    $draft_component->version_id = $version->id;
                    $draft_component->component_id = $component->component_id;

                    $draft_component->save();
                }

                $response['message'] = 'Draft created.';

                DB::commit();
            }
            catch (Exception $e)
            {
                DB::rollback();
                $response['message'] = $e->getMessage();
            }
        }
        else {
            $response['message'] = 'Error: Draft already exists.';
        }

        return Redirect::to($input['uri'] . '?edit=draft')->with('message', $response['message']);

    }

    public function submitDraft() {
        $input = Input::all();

        $uri = $input['uri'];
        $authorid = $input['author_id'];
        $pageid = $input['page_id'];

        try {
            $notification = Publishing::create(['page_id' => $pageid, 'author_id' => $authorid, 'status' => 'submission']);

            $response['message'] = 'Draft submitted to publishers.';
        }
        catch(Exception $e) {
            $response['message'] = $e->getMessage();
        }

        // Send publisher mail notifications

        $users = User::all();
        $page = Page::find($pageid);
        $mailtoarray = [];

        foreach($users as $user) {
            foreach ($user->roles as $role) {
                if ($page->roles->contains($role->id)) {
                    foreach ($role->permissions as $perm) {
                        if ($perm->name == 'publish') {

                            $mailtoarray[$user->username] = $user->first_name.' '.$user->last_name;
                        }
                    }
                }
            }
        }
        Mail::send('larafish::emails.draft-submission', [ 'page' => $page ], function($message) use ($mailtoarray, $page)
        {
            $message->subject('Draft Submission for ' . $page->title);
            $message->from(config('mail.from.address'), 'Publishing Notifications');
            foreach($mailtoarray as $email=>$name){
                $message->to($email, $name);
            }
        });

        return Redirect::to($uri . '?edit=draft')->with('message', $response['message']);
    }

    public function unsubmitDraft() {
        $input = Input::all();

        $uri = $input['uri'];
        $page_id = $input['page_id'];

        try {
            $notification = Publishing::where('page_id', $page_id)->where('status', 'submission')->delete();

            $response['message'] = 'Draft unlocked.';
        }
        catch(Exception $e) {
            $response['message'] = $e->getMessage();
        }

        return Redirect::to($uri . '?edit=draft')->with('message', $response['message']);
    }

    public function declineDraft() {
        $input = Input::all();

        $uri = $input['uri'];
        $page_id = $input['page_id'];
        $publisher_id = $input['publisher_id'];
        $comments = $input['comments'];

        try {
            $notification = Publishing::where('page_id', $page_id)->where('status', 'submission')->firstOrFail();

            $notification->status = 'declined';
            $notification->publisher_id = $publisher_id;
            $notification->comments = $comments;

            $notification->save();

            # Notify author or decline
            $page = Page::find($page_id);
            $author = User::find($notification->author_id);
            Mail::send('larafish::emails.draft-declined', [ 'page' => $page, 'comments' => $comments ], function($message) use ($author, $page)
            {
                $message->subject('Draft Declined for ' . $page->title);
                $message->from(config('mail.from.address'), 'Publishing Notifications');
                $message->to($author->username, $author->first_name . ' ' . $author->last_name);
            });

            $response['message'] = 'Draft declined.';
        }
        catch(Exception $e) {
            Bugsnag::notifyException($e);
            $response['message'] = $e->getMessage();
        }

        return Redirect::to($uri . '?edit=draft')->with('message', $response['message']);
    }

    public function deleteDraft() {

        $input = Input::all();

        $uri = $input['uri'];
        $pageid = $input['page_id'];

        try {

            DB::beginTransaction();

            $version = Version::where('page_id', $pageid)->where('status', 'draft')->firstOrFail();

            $components = Component::where('version_id', $version->id)->delete();

            # Delete notifications
            Publishing::where('page_id', $version->page_id)->delete();

            $version->delete();

            $response['message'] = 'Draft deleted.';

            DB::commit();
        }
        catch (Exception $e)
        {
            DB::rollback();
            Bugsnag::notifyException($e);
            $response['message'] = $e->getMessage();
        }

        return Redirect::to($uri);
    }

    public function publishDraft() {

        $input = Input::all();

        $uri = $input['uri'];
        $version_id = $input['version_id'];

        try {
            DB::beginTransaction();

            # Get publisher info

            $larafish = new Larafish;

            $publisher = $larafish->user();

            # Get current draft version

            $version_to_publish = Version::find($version_id);

            # Get current active version

            $active_version = Version::where('page_id', $version_to_publish->page_id)->where('status', 'active')->firstOrFail();

            # Set draft version as active

            $version_to_publish->status = 'active';
            $version_to_publish->save();

            # Remove the version that was previously active
            $active_version->delete();

            $notification = Publishing::where('page_id', $version_to_publish->page_id)->where('status', 'submission')->first();

            if(isset($notification)) {
                $notification->status = 'published';
                $notification->publisher_id = $publisher->id;
                $notification->save();
            }

            $response['message'] = 'Draft published.  Old version preserved as draft.';

            # Delete notifications
            $previousNotifications = Publishing::where('page_id', $version_to_publish->page_id);
            if(! is_null($notification))
            {
                $previousNotifications->where('id', '!=', $notification->id);
            }
            $previousNotifications->delete();

            # Notify author of approval
            $page = $version_to_publish->page;

            if($notification)
            {
                $author = User::find($notification->author_id);

                if($author)
                {
                    Mail::send('larafish::emails.draft-published', [ 'page' => $page ], function($message) use ($author, $page)
                    {
                        $message->subject('Draft Published for ' . $page->title);
                        $message->from(config('mail.from.address'), 'Publishing Notifications');
                        $message->to($author->username, $author->first_name . ' ' . $author->last_name);
                    });
                }
            }

            # Clear page cache
            Cache::forget('page:'.$page->uri.':published');

            DB::commit();
        }
        catch(Exception $e) {

            DB::rollback();
            Bugsnag::notifyException($e);
            $response['message'] = $e->getMessage();
        }

        return Redirect::to($uri);
    }

    public function deleteNotification() {
        $input = Input::all();

        $uri = $input['uri'];
        $notification_id = $input['notification_id'];

        try {
            $notification = Publishing::find($notification_id);

            $notification->delete();

            $response['message'] = 'Notification removed';
        }
        catch(Exception $e) {
            $response['message'] = $e->getMessage();
        }

        return Redirect::to($uri);
    }
}