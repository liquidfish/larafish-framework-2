<?php namespace Liquidfish\Larafish\Page\Version;

use App;
use Auth;
use Larafish;
use Illuminate\Support\Str;
use View;

use Liquidfish\Larafish\Page\Version\Publishing as Publishing;

class Version extends \Eloquent
{
    protected $guarded = array('id');
    public $table = 'page_versions';
    protected $fillable = array('page_id','status');


    /**
     * Components relationship
     * @return object Related components
     */
    public function components()
    {
        return $this->hasMany('Liquidfish\Larafish\Page\Component\Component','version_id');
    }

    /**
     * Page relationship
     * @return object Related page
     */
    public function page()
    {
        return $this->belongsTo('Liquidfish\Larafish\Page\Page','page_id');
    }

    public function isLocked() {
        if($this->status == 'draft') {
            # Check if a draft exists and if it has been submitted for review
            $submission = Publishing::where('page_id', $this->page_id)->where('status', 'submission')->get();

            if($submission->count())
                return true;
        }

        return false;
    }

    /**
     * @return Full name of author who locked Version
     */

    public function lockedBy() {
        if($this->isLocked()) {
            $submission = Publishing::where('page_id', $this->page_id)->where('status', 'submission')->firstOrFail();

            return $submission->author;
        }
    }
}