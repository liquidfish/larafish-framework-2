<?php namespace Liquidfish\Larafish\Page\Version;

use App;
use Auth;
use Larafish;
use Illuminate\Support\Str;
use View;

class Publishing extends \Eloquent
{
    protected $guarded = array('id');
    public $table = 'publishing_notifications';

    /**
     * Page relationship
     * @return object Related page
     */
    public function page()
    {
        return $this->belongsTo('Liquidfish\Larafish\Page\Page','page_id');
    }

    /**
     * User/Author relationship
     * @return object Related user
     */
    public function author()
    {
        return $this->belongsTo('Liquidfish\Larafish\Models\User','author_id');
    }

    /**
     * User/Publisher relationship
     * @return object Related user
     */
    public function publisher()
    {
        return $this->belongsTo('Liquidfish\Larafish\Models\User','publisher_id');
    }



}