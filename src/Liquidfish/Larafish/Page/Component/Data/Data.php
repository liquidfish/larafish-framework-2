<?php namespace Liquidfish\Larafish\Page\Component\Data;

class Data extends \Eloquent {
	protected $guarded = array('id');
	protected $table = 'page_component_data';
}