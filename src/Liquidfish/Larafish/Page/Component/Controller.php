<?php namespace Liquidfish\Larafish\Page\Component;

use Input;

class Controller extends \Illuminate\Routing\Controller {

	protected $componentRepository;

	function __construct(Repository $componentRepository)
	{
		$this->componentRepository = $componentRepository;
	}

	public function store()
	{
		$pageId = Input::get('page_id');
		$componentId = Input::get('component_id');
		$versionId = Input::get('version_id');
		return $this->componentRepository->add($pageId, $componentId, $versionId);
	}

	public function destroy()
	{
		$pageComponentId = Input::get('page_component_id');
		return $this->componentRepository->remove($pageComponentId);
	}

} 