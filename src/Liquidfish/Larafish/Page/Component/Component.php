<?php namespace Liquidfish\Larafish\Page\Component;

use App;
use Auth;
use Larafish;
use Illuminate\Support\Str;
use View;

class Component extends \Eloquent {

	protected $guarded = array('id');
	public $table = 'page_components';

	public function component()
	{
		return $this->belongsTo('Liquidfish\Larafish\Component\Component','component_id');
	}

	public function data()
	{
		return $this->belongsTo('Liquidfish\Larafish\Page\Component\Data\Data','page_component_data_id');
	}

	public function version()
	{
		return $this->belongsTo('Liquidfish\Larafish\Page\Version\Version', 'version_id');
	}


	/*
	----------------------------------------------------------
		Components
	----------------------------------------------------------
	*/

	/**
	 * Create the markup for displaying a PageComponent
	 * @return string html
	 */
	public function open()
	{
		return '<div data-component="'.$this->component->id.'" data-component-data-id="'.$this->data->id.'">';
	}

	/**
	 * Closing markup for a PageComponent
	 * @return string html
	 */
	public function close()
	{
		return '</div><!-- component '.$this->component->view.' -->';
	}

	/**
	 * Display an editable slot of data in a component view
	 *
	 * Essentially a contenteditable div utilizing a CKEditor
	 *
	 * @param  string $slot    Key name for slot data stored in PageComponentData
	 * @param  array  $options Settings for CKEditor
	 * @return string          HTML markup for slot
	 */

	public function slot($slot = null, $options = array('no-paragraph' => false))
	{
		$id = $slot.'_'.Str::quickRandom(3);

		$markup = '';
		$markup .= '<div data-slot='.$slot.' id="'.$id.'" class="editor '.($options['no-paragraph'] == true ? 'no-paragraph' : '').'" '.(Larafish::userCanEditVersion($this->version->id) ? 'contenteditable="true"' : '').'>';

		$markup .= $this->dataForSlot($slot);

		$markup .= '</div>';

		return $markup;
	}

	/**
	 * Display an editable slot of data in a component view
	 *
	 * Essentially a contenteditable div utilizing a CKEditor
	 *
	 * @param  string $slot    Key name for slot data stored in PageComponentData
	 * @param  array  $options Settings for CKEditor
	 * @return string          HTML markup for slot
	 */
	public function photoSlot($slot = null, $options = array('size' => '100x100', 'class' => ''))
	{
		$id = $slot.'_'.Str::random(10);

		$markup = '';

		$markup .= '<div class="'.$options['class'].'" id="photo_'.$id.'" style="'.$this->dataForSlot($slot).'" data-slot="'.$slot.'" data-type="style"></div>';

		$larafish = App::make('larafish');

		if($larafish->userIsLoggedIn)
		{
			$markup .= '<div type="file" id="uploader_'.$id.'" class="change-photo" data-photo-size="'.$options['size'].'" data-target="photo_'.$id.'" data-target-type="style" style="position:absolute"></div>';
		}

		return $markup;
	}

	/**
	 * Get the associated key from PageComponentData for the specified slot
	 * @param  string $slot    Key in PageComponentData array
	 * @param  string $default Placeholder data if key for slot does not exist
	 * @return string          String of data for slot
	 */
	public function dataForSlot($slot = null, $default = 'empty')
	{
		$data = json_decode($this->data->data,true);

		$slot_data = $default;

		if( isset($data[$slot]) )
		{
			$slot_data = $data[$slot];
		}

		return $slot_data;
	}

    /* Allow for rendering components in components <?= $component->render('component-name') ?>
     * @param string $title name of the component
     * @return string View rendered as a markup string
    */
    public function render($title = null)
    {
        return View::make("components.{$title}")->render();
    }
}
