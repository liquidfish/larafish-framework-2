<?php namespace Liquidfish\Larafish\Page\Navigation;

use Cache, Larafish, stdClass;
use Liquidfish\Larafish\Page\Repository as PageRepository;

class Navigator {

	public $pageRepository;

	function __construct(PageRepository $pageRepository)
	{
		$this->pageRepository = $pageRepository;
	}

	/**
	 * Generates page structure array with pages as objects
	 * @param $for_uri null|string Uri to limit results by
	 * @return array
	 */
	public function map($for_uri = null)
	{
		# 5.3 support
		$self = $this;

		# Caching check
		$page_map = Cache::rememberForever('pages', function() use ($self)
		{

			$pages = array();

			$data = $self->pageRepository->allPagesByType();

			foreach($data->primaryPages as $primary_page)
			{
				# Build secondary pages
				$primary_object = new stdClass;
				$primary_object->id = $primary_page->id;
				$primary_object->title = $primary_page->title;
				$primary_object->nav_title = $primary_page->nav_title;
				$primary_object->uri = $primary_page->uri;
				$primary_object->published = $primary_page->published;
				$primary_object->redirect_url = $primary_page->redirect_url;
				$primary_object->hidden = $primary_page->hidden;

				if($primary_page->roles->count())
				{
					$primary_object->required_roles = $primary_page->roles->lists('name');
				}
				else
				{
					$primary_object->required_roles = array();
				}

				foreach($data->secondaryPages as $secondary_page)
				{

					if($secondary_page->primary_uri == $primary_page->primary_uri)
					{
						$secondary_object = new stdClass;
						$secondary_object->id = $secondary_page->id;
						$secondary_object->title = $secondary_page->title;
						$secondary_object->nav_title = $secondary_page->nav_title;
						$secondary_object->uri = $secondary_page->uri;
						$secondary_object->published = $secondary_page->published;
						$secondary_object->redirect_url = $secondary_page->redirect_url;
						$secondary_object->hidden = $secondary_page->hidden;

						if($secondary_page->roles->count())
						{
							$secondary_object->required_roles = $secondary_page->roles->lists('name');
						}
						else
						{
							$secondary_object->required_roles = array();
						}

						foreach($data->tertiaryPages as $tertiary_page)
						{

							if($tertiary_page->secondary_uri == $secondary_page->secondary_uri)
							{
								$tertiary_object = new stdClass;
								$tertiary_object->id = $tertiary_page->id;
								$tertiary_object->title = $tertiary_page->title;
								$tertiary_object->nav_title = $tertiary_page->nav_title;
								$tertiary_object->uri = $tertiary_page->uri;
								$tertiary_object->published = $tertiary_page->published;
								$tertiary_object->redirect_url = $tertiary_page->redirect_url;
								$tertiary_object->hidden = $tertiary_page->hidden;

								if($tertiary_page->roles->count())
								{
									$tertiary_object->required_roles = $tertiary_page->roles->lists('name');
								}
								else
								{
									$tertiary_object->required_roles = array();
								}

								$secondary_object->subpages[] = $tertiary_object;

							}

						}
						$primary_object->subpages[] = $secondary_object;

					}

				}
				if($primary_page->primary_uri != '')
				{
					$pages[$primary_page->primary_uri] = $primary_object;
				}
				else
				{
					$pages['home'] = $primary_object;
				}
			}

		 	return $pages;

		});

		if(!is_null($for_uri))
		{
			if(isset($page_map[$for_uri]))
			{
				return array($for_uri => $page_map[$for_uri]);
			}
		}
		else
		{
			return $page_map;
		}

	}

	/**
	 * Generate an ordered list as a navigation
	 * @param  string $for_uri Limit results to the specified uri
	 * @param  array  $options [description]
	 * @return string          HTML Markup of navigation
	 */
	public function markup($for_uri = null, $options = array('wrapped' => true))
	{
		# Check user ability
		$admin = false;
		if(Larafish::userIsLoggedIn())
		{
			if(Larafish::user()->ability('Admin','edit_pages'))
			{
				$admin = true;
			}
		}

		# Get the full page map
		$pages = $this->map($for_uri);

		$markup = '';

		if(count($pages))
		{

			if($options['wrapped'] === true)
			{
				$markup .= "<ul>\n";
			}

			foreach($pages as $page)
			{

				if(Larafish::userHasRole($page->required_roles))
				{
					$markup .= "<li>\n";

					$markup .= '<a href="'.( ($page->redirect_url != null and !$admin) ? $page->redirect_url : $page->uri ).'">'.$page->nav_title."</a>\n";

					if(isset($page->subpages))
					{

						$markup .= "<ul class=\"secondary\">\n";

						foreach($page->subpages as $secondary_page)
						{

							if(Larafish::userHasRole($secondary_page->required_roles))
							{
								$markup .= "<li>\n";
								$markup .= '<a href="'.( ($secondary_page->redirect_url != null and !$admin) ? $secondary_page->redirect_url : $secondary_page->uri ).'">'.$secondary_page->nav_title."</a>\n";

								if(isset($secondary_page->subpages))
								{

									$markup .= "<ul class=\"tertiary\">\n";

									foreach($secondary_page->subpages as $tertiary_page)
									{
										if(Larafish::userHasRole($tertiary_page->required_roles))
										{
											$markup .= "<li>\n";
											$markup .= '<a href="'.( ($tertiary_page->redirect_url != null and !$admin) ? $tertiary_page->redirect_url : $tertiary_page->uri ).'">'.$tertiary_page->nav_title."</a>\n";
											$markup .= "</li>\n";
										}

									}

									$markup .= "</ul>\n";

								}

								$markup .= "</li>\n";
							}

						}

						$markup .= "</ul>\n";

					}

					$markup .= "</li>\n";

				}

			}
			if($options['wrapped'] === true)
			{
				$markup .= "</ul>\n";
			}


			return $markup;

		}
		else
		{
			return '<!-- No pages -->';
		}

	}
} 