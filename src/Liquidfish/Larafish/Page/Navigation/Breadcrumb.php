<?php namespace Liquidfish\Larafish\Page\Navigation;

use Route;
use URL;
use Liquidfish\Larafish\Page\Page;

class Breadcrumb {

	protected $rootTitle = 'Home';
	protected $rootRoute;

	/**
	 * Call it something other than Home
	 * @param string $title
	 */
	public function setRootTitle($title)
	{
		$this->rootTitle = $title;
	}

	/**
	 * Set the root route
	 * @param mixed $route
	 */
	public function setRootRoute($route)
	{
		if(is_string($route))
		{
			$this->rootRoute = URL::to($route);
		}
		if(is_object($route))
		{
			$this->rootRoute = $route;
		}
	}

	/**
	 * @param Page $page
	 * @return array
	 */
	public function forPage(Page $page)
	{

		if(empty($this->rootRoute))
		{
			$this->rootRoute = URL::route('home');
		}

		$crumbs = array(
			$this->rootTitle => $this->rootRoute
		);

		# Load parent page's
		$parents = array_reverse($page->parents());

		foreach($parents as $parent)
		{
			$params = array();
			if(!empty($parent->primary_uri)) $params[] = $parent->primary_uri;
			if(!empty($parent->secondary_uri)) $params[] = $parent->secondary_uri;
			if(!empty($parent->tertiary_uri)) $params[] = $parent->tertiary_uri;
			$crumbs[$parent->nav_title] = URL::route('page', $params);
			unset($params);
		}

		return $crumbs;

	}

} 