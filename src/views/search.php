<div class="larafish">
	<h1>Search Results for "<?= $query ?>"</h1>

	<? if(count($results)): ?>
		<? foreach($results as $result): ?>
			<a href="<?= $result['uri'] ?>"><?= $result['title'] ?></a>
			<p><?= $result['content'] ?></p>
		<? endforeach ?>
	<? else: ?>
		<p>No results found</p>
	<? endif ?>
</div>