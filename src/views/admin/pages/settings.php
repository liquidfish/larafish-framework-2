<div id="page_settings">
	<div class="ui fluid accordion">
		<div class="title">
			<i class="dropdown icon"></i> Page Settings
		</div>
		<div class="content">

			<div class="ui grid">
				<div class="eleven wide column">
					<?= Form::model($page, array('route' => array('admin.pages.update', $page->id), 'method' => 'put', 'class' => 'ui form segment')) ?>

					<div class="field">
						<?= Form::label('Title') ?>
						<?= Form::text('title') ?>
					</div>

					<div class="field">
						<?= Form::label('Published') ?>
						<?= Form::hidden('published',0) ?>
						<?= Form::checkbox('published', '1') ?>
					</div>

					<div class="field">
						<?= Form::label('View') ?>
						<?= Form::select('view', $available_views) ?>
						<? if(isset($message)) echo '<p>'.$message.'</p>' ?>
					</div>

					<div class="field">
						<?= Form::label('Redirect URL') ?>
						<?= Form::text('redirect_url') ?>
					</div>

					<?= Form::button('Update Settings',array('type' => 'submit', 'class' => 'ui green mini submit button')) ?>

					<?= Form::close() ?>
				</div>
				<div class="five wide column">
					<div class="ui segment">
						<h3>DangerZone</h3>
						<?= Form::model($page, array('route' => array('admin.pages.destroy', $page->id), 'method' => 'delete', 'class'                                                            => 'ui form')) ?>
							<p>Confirm deletion by typing "Delete" below</p>
							<div class="field">
								<input type="text" name="confirm"/>
							</div>
							<button class="ui mini primary submit button"><i class="trash icon"></i> Delete this Page</button>
						<?= Form::close() ?>
					</div>
				</div>
			</div>

			<? if(isset($component_settings)) echo $component_settings ?>
			<? if(isset($role_settings)) echo $role_settings ?>

		</div>

	</div>

</div>