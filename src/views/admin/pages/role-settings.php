<div class="ui segment">

    <h3>Current Page Editor Roles</h3>

    <table class="ui table segment" data-current-roles>
        <thead>
        <tr>
            <th>Role</th>
        </tr>
        </thead>
        <tbody>

        <? foreach($page_roles as $role): ?>
            <tr>
                <td><?= $role->name ?></td>
                <td>
                    <a class="ui tiny red button right floated" data-page-id="<?= $page_id ?>" data-role-id-to-remove="<?= $role->id ?>"><i class="remove icon"></i>Remove</a>
                </td>
            </tr>
        <? endforeach ?>
        </tbody>
    </table>

    <h3>Available Page Editor Roles</h3>

    <table class="ui table segment" data-available-roles>
        <thead>
            <tr>
                <th>Role</th>
            </tr>
        </thead>
        <tbody>
        <? foreach($roles as $role): ?>
            <? if(!$page_roles->contains($role->id)): ?>
                <tr>
                    <td><?= $role->name ?></td>
                    <td>
                        <a class="ui tiny green button right floated" data-page-id="<?= $page_id ?>" data-role-id-to-add="<?= $role->id ?>"><i class="add icon"></i>Add</a>
                    </td>
                </tr>
            <? endif ?>
        <? endforeach ?>
        </tbody>
    </table>

</div>