<div class="larafish">
	<div class="ui secondary segment">

		<div class="ui grid">
			<div class="left floated three wide column">
				<a class="ui small green button" href="/admin/components/sync">
					<i class="refresh icon"></i> Sync Components
				</a>
			</div>
			<div class="right floated four wide column">
				<form action="" class="ui fluid icon input" method="get">
					<input type="text" name="query" placeholder="Search..." value="<?= Input::get('query') ?>">
					<i class="search link icon"></i>
				</form>
			</div>
		</div>

		<div class="ui header">
			Components
			<div class="ui sub header">
				Synced Component views in the resources/views/components directory of your project
			</div>
		</div>
		<? if($components->count()): ?>

		<table class="ui small table segment">
		<thead>
			<tr>
				<th>View</th>
			</tr>
		</thead>
		<tbody>
		<? foreach($components as $component): ?>
		<tr class="">
			<td><?= $component->view ?></td>
		</tr>
		<? endforeach ?>
		</tbody>
		</table>
		<?= view('larafish::pagination', [ 'paginator' => $components ]) ?>

		<? else: ?>

		<h3>No components found.</h3>

		<? endif; ?>
	</div>
</div>
