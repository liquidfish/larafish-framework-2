<div id="publishing_notifications">
    <div class="ui fluid accordion">
        <div class="title">
            <i class="dropdown icon"></i> Publishing <? if($larafish->getUserNotificationCount()): ?><div class="ui horizontal label"><?= $larafish->getUserNotificationCount() ?></div><? endif ?>
        </div>
        <div class="content">
            <div class="ui grid">
                <div class="five wide column">
                    <? if($version->status == 'draft' && $larafish->userCanPublish($page->id)): ?>
                        <div class="ui segment">
                            <h4 class="ui grey header">Publisher Actions</h4>
                            <?
                            echo Form::open(array('url' => '/admin/versions/publish', 'class' => 'ui form'));
                            echo Form::hidden('version_id', $version->id);
                            echo Form::hidden('uri', Request::path());
                            echo '<button type="submit" class="ui fluid green submit button">Publish this Draft</button>';
                            echo Form::close();

                            if($version->isLocked()) {
                                echo '<div class="ui horizontal divider">Or</div>';

                                echo Form::open(array('url' => '/admin/versions/decline', 'style' => 'display: inline-block', 'class' => 'ui form'));
                                echo Form::hidden('page_id', $page->id);
                                echo Form::hidden('uri', Request::path());
                                echo Form::hidden('publisher_id', $larafish->user()->id);
                                echo '<div class="field">';
                                echo Form::textarea('comments', NULL, array('placeholder' => 'Decline this draft and give a brief description of why'));
                                echo '<br><br><button type="submit" class="ui fluid orange submit button">Decline this Draft</button>';
                                echo '</div>';
                                echo Form::close() . '<br><br>';
                            }
                            ?>
                        </div>
                    <? endif ?>
                    <?
                    if($larafish->userCanAuthor($page->id)) {
                        echo '<div class="ui segment"><h4 class="ui grey header">Author Actions</h4>';
                        if (!$draft_exists) {
                            echo Form::open(array('url' => '/admin/versions/create'));
                            echo Form::hidden('page_id', $page->id);
                            echo Form::hidden('uri', Request::path());
                            echo '<div class="ui message">To make changes, create a new draft.</div>';
                            echo '<button type="submit" class="ui fluid primary submit button">Create new Draft</button>';
                            echo Form::close();
                        }
                        else {
                            if ($version->status == 'draft') {
                                if($version->isLocked()) {
                                    echo '<div class="ui message">This draft version has been submitted for publishing and is locked.</div>';

                                    # If this version is locked by you, offer an unlocking option.
                                    echo '<div class="ui message">Unlocking draft will remove draft from submission.</div>';
                                    echo Form::open(array('url' => '/admin/versions/unsubmit', 'class' => 'ui form'));
                                    echo Form::hidden('page_id', $page->id);
                                    echo Form::hidden('uri', Request::path());
                                    echo '<button type="submit" class="ui fluid red submit button" '.($version->lockedBy()->id != $larafish->user()->id ? 'disabled' : '' ).'>Unlock this Draft</button>';
                                    echo Form::close();
                                }
                                else {
                                    echo Form::open(array('url' => '/admin/versions/delete', 'class' => 'ui form'));
                                    echo Form::hidden('page_id', $page->id);
                                    echo Form::hidden('uri', Request::path());
                                    echo '<button type="submit" class="ui fluid red submit button">Delete this Draft</button>';
                                    echo Form::close();

                                    echo Form::open(array('url' => '/admin/versions/submit', 'style' => 'display: inline-block'));
                                    echo Form::hidden('author_id', $larafish->user()->id);
                                    echo Form::hidden('page_id', $page->id);
                                    echo Form::hidden('uri', Request::path());
                                    echo '<div class="ui message">Draft will be locked and a publisher will be notified that you wish to publish this page.</div>';
                                    echo '<button type="submit" class="ui fluid teal submit button">Submit draft to Publisher</button>';
                                    echo Form::close();
                                }
                            }
                            else {
                                echo '<a href="' . (Request::path() === '/' ? '' : '/'.Request::path()) . '?edit=draft"><button type="submit" class="ui fluid primary submit button" style="display: inline-block;">View Draft</button></a>';
                            }
                        }
                        echo '</div>';
                    }
                    ?>
                </div>
                <div class="eleven wide column">
                    <div class="ui segment">
                        <h4 class="ui grey header">Publishing information for "<?= $page->title ?>"</h4>
                        <div class="ui equal width grid">
                            <div class="column">
                                <b>Version</b>
                                <?
                                if($version->status == 'draft')
                                    echo '<div class="ui info message"><div class="header">Draft</div>This is the draft version.<br>(<a href="' . Request::url() . '"> Visit live version</a> )</div>';
                                else
                                    echo '<div class="ui success message"><div class="header">Live</div>This is the version displayed to the public.</div>';
                                ?>
                            </div>
                            <div class="column">
                                <b>Status</b><br>
                                <?
                                if($version->status == 'draft') {
                                    if($version->isLocked()) {
                                        echo '<div class="ui error message"><div class="header">Under Review</div>';
                                        echo '<br>This draft version has been submitted for publishing and is locked.<br><br>Unlocking draft will remove draft from submission.<br><br>Draft submitted by ' . $version->lockedBy()->fullname() . '</div>';
                                    }
                                    else
                                        echo '<div class="ui success message"><div class="header">Draft ready for edits</div></div>';
                                }
                                else {
                                    if($draft_exists && $awaiting_approval)
                                    {
                                        echo '<div class="ui info message"><div class="header">Draft awaiting approval</div>';
                                        echo '<a href="' . (Request::path() === '/' ? '' : '/'.Request::path()) . '?edit=draft">Please review the draft</a></div>';
                                    }
                                    else if($draft_exists && !$awaiting_approval)
                                    {
                                        echo '<div class="ui message"><a href="' . (Request::path() === '/' ? '' : '/'.Request::path()) . '?edit=draft">View draft version</a></div>';
                                    }
                                    else
                                    {
                                        echo '<div class="ui success message"><div class="header">Ready for a new draft!</div></div>';
                                    }
                                }
                                ?>
                                <br>
                            </div>
                        </div>
                    </div>

                    <div class="ui segment">
                        <h4 class="ui grey header"> Notifications</h4>
                        <? if($larafish->userCan('publish')): ?>

                            <? foreach($larafish->getUserPublisherNotifications() as $submission): ?>
                                <div class="ui teal segment">
                                    <h4>Draft Waiting for Review</h4>
                                    <div class="ui horizontal segments">
                                        <div class="ui segment">
                                            <h5>Page</h5>
                                            <?= $submission->page->title ?>
                                        </div>
                                        <div class="ui segment">
                                            <h5>Author</h5>
                                            <?= $submission->author->fullname() ?>
                                        </div>
                                        <div class="ui segment">
                                            <h5>Submitted On</h5>
                                            <?= $submission->created_at ?>
                                        </div>
                                        <div class="ui segment center aligned">
                                            <a href="<?= $submission->page->getUriAttribute() ?>?edit=draft" class="ui small button teal">View</a>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach ?>
                        <? endif ?>
                        <? if($larafish->userCan('author')): ?>
                            <? foreach($larafish->getUserAuthorNotifications() as $submission): ?>
                                <div class="ui <?= ($submission->status == 'DECLINED' ? 'red' : 'green') ?> segment">
                                    <h4><?= ($submission->status == 'DECLINED' ? 'Draft Declined' : 'Draft Published') ?></h4>
                                    <div class="ui horizontal segments">
                                        <div class="ui segment">
                                            <h5>Page</h5>
                                            <?= $submission->page->title ?>
                                        </div>
                                        <div class="ui segment">
                                            <h5>Publisher</h5>
                                            <?= $submission->publisher->fullname() ?>
                                        </div>
                                        <div class="ui segment">
                                            <h5><?= ($submission->status == 'DECLINED' ? 'Declined' : 'Published' ) ?> On</h5>
                                            <?= $submission->updated_at ?>
                                        </div>
                                        <div class="ui segment">
                                            <?= Form::open(array('url' => '/admin/versions/deleteNotification', 'class' => 'ui form')); ?>
                                            <?= Form::hidden('notification_id', $submission->id); ?>
                                            <?= Form::hidden('uri', Request::path()); ?>
                                            <?= '<button type="submit" class="ui mini red submit button">Delete Notification</button>'; ?>
                                            <?= Form::close(); ?>
                                        </div>
                                    </div>
                                    <? if($submission->status == 'DECLINED'): ?>
                                        <div class="ui segment">
                                            <h5>Publisher Comments</h5>
                                            <?= $submission->comments ?>
                                        </div>
                                    <? endif ?>
                                </div>
                            <? endforeach ?>
                        <? endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>