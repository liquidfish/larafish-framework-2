<div class="larafish">

	<h1>Edit Role</h1>

	<?
	echo Form::model($role, array('method' => 'put', 'route' => array('admin.roles.update', $role->id), 'id' => 'form'));

	echo Form::label('name','Name');
	echo Form::text('name',$role->name,array('required' => 'required'));

	echo '<h2>Permissions</h2>';
	echo '<div>';

	foreach($permissions as $permission)
	{
		echo Form::checkbox('perms[]',$permission->id, $role->permissions->contains($permission->id), array('id' => 'permission_'.$permission->id));

		echo '<label for="permission_'.$permission->id.'">'.$permission->display_name.'</label>';
	}

	echo '</div>';

	echo Form::button('Save Changes',array('type' => 'submit'));

	echo Form::close();

	# Delete
	echo Form::model($role, array('route' => array('admin.roles.destroy', $role->id),'method' => 'delete'));
	echo Form::button('Delete Role',array('type' => 'submit'));
	echo Form::close();
	?>
</div>