<div class="larafish">
	<div class="ui secondary segment">

		<div class="ui grid">
			<div class="left floated three wide column">
				<a class="ui small green button" href="<?= URL::route('admin.roles.create') ?>">
					<i class="plus icon"></i> Create Role
				</a>
			</div>
			<div class="right floated four wide column">
				<form action="" class="ui fluid icon input" method="get">
					<input type="text" name="query" placeholder="Search..." value="<?= Input::get('query') ?>">
					<i class="search link icon"></i>
				</form>
			</div>
		</div>

		<table class="ui small table segment">
			<thead>
				<tr>
					<th></th>
					<th>Role</th>
					<th>Permissions</th>
				</tr>
			</thead>
			<tbody>
				<? foreach($roles as $role): ?>
				<tr>
					<td><a href="<?= URL::route('admin.roles.edit', $role->id) ?>" class="ui mini button">Edit</a></td>
					<td><?= $role->name ?></td>
					<td>
					<? foreach($role->permissions as $permission): ?>
						<?= $permission->display_name ?><?= $role->permissions->last()->display_name ==  $permission->display_name ? '' : ', ' ?>
					<? endforeach ?>
					</td>
				</tr>
				<? endforeach ?>
			</tbody>
		</table>

		<?= view('larafish::pagination', [ 'paginator' => $roles ]) ?>

	</div>
</div>