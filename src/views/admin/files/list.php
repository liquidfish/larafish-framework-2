<? if(count($files) > 0): ?>
<form class="pure-form" method="post" action="<?= URL::route('admin.files.change').$create_uri ?>" id="change">

	<input type="hidden" name="_token" value="<?= csrf_token() ?>">

	<div class="pure-menu pure-menu-open pure-menu-horizontal">
		<a href="#" class="pure-menu-heading">Actions</a>
		<ul>
			<li>
				<button type="button" id="delete" disabled="disabled" class="delete-files pure-button pure-button-primary pure-button-xsmall">Delete</button>
			</li>
			<li>
				<input type="hidden" name="operation" value="">
				<button type="button" id="move" disabled="disabled"  class="move-files pure-button pure-button-xsmall" >Move To</button>
				<select name="move_to" id="">
					<option value="">-</option>
					<option value="/">/</option>
				<? foreach($available_directories as $dir)
				{
					$path = str_replace(app_path().'/uploads', '', $dir->getRelativePathname());
					echo '<option value="'.$path.'">'.$path.'</option>';
				} ?>
				</select>
			</li>
		</ul>
	</div>

<table class="pure-table pure-table-bordered">
	<thead>
		<tr>
			<th></th>
			<th>Select</th>
			<th>Name</th>
		</tr>
	</thead>
	<tbody>
<?
$i = 0;
foreach($files as $file)
{
	echo '<tr class="'.(($i%2 == 0) ?: "pure-table-odd").'">';
	switch ($file->getType()) {
		case 'dir':
			echo '<td>';
				echo '<img src="/images/icons-folder.png">';
			echo '</td>';
			echo '<td>';
				echo '<input type="checkbox" name="directories[]" value="'.$file->getRelativePathname().'">';
			echo '</td>';
			echo '<td>';
				// echo $current_path_uri;
				if($current_path_uri != '')
				{
					$file_path = $current_path_uri.'.'.$file->getRelativePathname();
				} else 
				{
					$file_path = $file->getRelativePathname();
				}
				echo '<a href="/admin/files/browser?path='.$file_path.'">'.$file->getRelativePathname().'</a>';
			echo '</td>';
			break;

		case 'fImage':
			echo '<td>';
				// echo 'image';
				echo '<img src="/file/'.$guid.'" style="width: 30px; height: auto; display: block; border: 1px solid #7b7b7b;">';
			echo '</td>';
			echo '<td>';
				echo '<input type="checkbox" name="files[]" value="'.$file->getName().'">';
			echo '</td>';
			echo '<td>';
				echo '<a href="'.str_replace(public_path().'/uploads/', '', $file->getPathName()).'" class="choose-file">'.SecureDocument::getHumanNameFromFilename($file->getName()).'</a>';
			echo '</td>';
			echo '</td>';
			break;
			
		default:
			echo '<td>';
				echo '<img src="/images/icons-file.png">';
			echo '</td>';
			echo '<td>';
				echo '<input type="checkbox" name="files[]" value="'.$file->getRelativePathname().'">';
			echo '</td>';
			echo '<td>';
				echo '<a href="/uploads/'.str_replace(public_path().'/uploads/', '', $file->getPathName()).'" class="choose-file">'.$file->getRelativePathname().'</a>';
			echo '</td>';
			break;
	}
	echo '<tr>';
	$i++;
}
?>
		</tbody>
	</table>
	
</form>

<? else: ?>
<div class="yui3-alert">
	No documents in this directory	
</div>
<? endif ?>