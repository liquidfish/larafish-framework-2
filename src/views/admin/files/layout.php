<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>File Browser</title>
	<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.2.1/pure-min.css">
	<link rel="stylesheet" href="/css/cssextras.css">
	<link rel="stylesheet" href="/css/pure-skin.css">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<style>
	body {
		padding: 20px;
	}
	table {
		width: 100%;
		margin-bottom: 50px;
	}
	table tr td:first-child {
		width: 20%;
	}
	table tr td:first-child img {
		margin-left: auto;
		margin-right: auto;
		display: block;
	}
	table tr td+td {
		width: 10%;
	}
	table tr td:last-child {
		width: 70%;
	}
	.new-actions {
		margin-top: 50px;
	}
	.pure-button-xsmall {
		font-size: 70%;
	}
	</style>
	<script>
	$(document).ready(function(){
		$('.delete-folder').on('click',function()
		{
			var r = confirm("Are you sure you want to delete this folder and all of its contents?");
			if (r == false){
				return false;
			}
		});
		<? if(Session::has("CKEditorFuncNum")): ?>
		$('.choose-file').click(function(){
			window.opener.CKEDITOR.tools.callFunction('<?= Session::get("CKEditorFuncNum") ?>',$(this).attr('href'));
			window.close();
		});
		<? endif ?>

		/*
		----------------------------------------------------------
			Move Files
		----------------------------------------------------------
		*/
		
		$('form#change').on('change','input[type="checkbox"]', function()
		{
			if($('form#change').find('input:checked').size() > 0)
			{
				$('#move,#delete').removeAttr('disabled');
			} else
			{
				$('#move,#delete').attr('disabled','disabled');
			}
		});

		$('form#change').on('click','button.move-files',function()
		{

			if($('form#change').find('input:checked').size() > 0)
			{	

				$('input[name="operation"]').val('move');

				var r = confirm("Are you sure you want to move these files?");
				if (r == false){
					return false;
				}

				$('form#change').submit();

			}

		});
		$('form#change').on('click','button.delete-files',function()
		{

			if($('form#change').find('input:checked').size() > 0)
			{

				$('input[name="operation"]').val('delete');

				var r = confirm("Are you sure you want to delete these files?");
				if (r == false){
					return false;
				}

				$('form#change').submit();

			}
		});
	});
	
	</script>
</head>
<body class="pure-skin-levy">
	<? if(Session::has('message')): ?>
	<div class="yui3-alert yui3-alert-error"><?= Session::get('message') ?></div>
	<? endif ?>
	<!-- <p>Current path: <?= $current_path_uri ?></p> -->
	<!-- <p>Create uri: <?= $create_uri ?></p> -->
	<h2>File Browser</h2>
	<div class="pure-menu pure-menu-open pure-menu-horizontal">
	    <a href="#" class="pure-menu-heading">Path</a>
		<ul>
		<? foreach($breadcrumbs as $breadcrumb): ?>
			<li><?= $breadcrumb ?></li>
		<? endforeach ?>
		</ul>
	</div>
	<div class="files">
		<?= $yield ?>
	</div>
	<div class="pure-g new-actions">

		<div class="pure-u-1-2">

			<form class="pure-form pure-form-stacked" action="/admin/files/upload<?= $create_uri ?>" method="post" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="<?= csrf_token() ?>">
				<fieldset>
					<legend>Upload Files</legend>
					<input type="file" name="files[]" style="margin-bottom: 5px" multiple>
					<button type="submit" id="upload" class="pure-button pure-button-success">Upload</button>
				</fieldset>
			</form>
		</div>
		<div class="pure-u-1-2">
			
			<form class="pure-form pure-form-stacked" action="/admin/files/create-directory<?= $create_uri ?>" method="post">
				<input type="hidden" name="_token" value="<?= csrf_token() ?>">
				<fieldset>
					<legend>Create Directories</legend>
					<input placeholder="Directory name" type="text" name="directory_name">
					<!-- <input type="submit" value="Create Directory"> -->
					<button type="submit" id="create" class="pure-button pure-button-success">Create Directory</button>
				</fieldset>
			</form>
		</div>
	</div>

</body>
</html>