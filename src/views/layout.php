<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title><?= $title ?> | Larafish</title>
	<link rel="shortcut icon" href="/favicon.ico">
	<?= Assets::output() ?>
</head>
<body>

	<? if(isset($admin_navigation)) echo $admin_navigation ?>
	<? if(isset($message)) echo $message ?>
	<? if(isset($page_settings)) echo $page_settings ?>

	<h1>Header</h1>

	<hr>

	<div id="page-content">
		<?= $yield ?>
	</div>
	
	<hr>

	<h3>Footer</h3>
</body>
</html>